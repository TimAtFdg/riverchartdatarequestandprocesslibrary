﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RiverChartDataRequestAndProcessLibrary
{
    public interface IChartDataRepository
    {
        //ChartData Get(string stationID, string PEType, int numberDaysOfData);
        ChartData Get(string stationID, string PEType);
    }
}
