﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RiverChartDataRequestAndProcessLibrary
{
    public interface IDataItemRepository
    {
        IEnumerable<SimpleDataItem> Get(string searchTerms, string PETypes);
        IEnumerable<SimpleDataItem> Get();
    }
}
