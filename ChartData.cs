﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Web;

namespace RiverChartDataRequestAndProcessLibrary
{
    public class ChartData
    {
        //public int NumberDaysOfData { get; set; }
        public bool DataRetrievedSuccessfully { get; set; }
        public string ErrorMessage { get; set; }
        public List<DataPoint> ObservedUnit1Data { get; set; }
        public List<DataPoint> ForecastUnit1Data { get; set; }
        public List<DataPoint> ObservedUnit2Data { get; set; }
        public List<DataPoint> ForecastUnit2Data { get; set; }
        public String TimeLabel { get; set; }
        public String YAxisLabelUnit1 { get; set; }
        public String YAxisLabelUnit2 { get; set; }
        public int UTCOffset { get; set; }
    }
}