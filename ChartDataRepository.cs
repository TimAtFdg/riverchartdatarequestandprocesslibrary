﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Web;
using System.Data.Entity;
//using System.Web.Configuration;
//using System.Configuration;
using System.Xml.Linq;
using System.Collections;

namespace RiverChartDataRequestAndProcessLibrary
{
    public class ChartDataRepository : IChartDataRepository
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("RiverChartDataRequestAndProcessLibrary.GeneralMessages");
        private static readonly log4net.ILog logSpecialProcessingMessage = log4net.LogManager.GetLogger("RiverChartDataRequestAndProcessLibrary.ProcessingMessages");

        private Hashtable yAxisLabels = new Hashtable() { 
                                                            {"stage", "River Stage (ft)"},
                                                            {"discharge", "Discharge (cfs)"},
                                                            {"inflow", "Inflow (cfs)"},
                                                            {"forebay_elevation", "Forebay Elevation (ft)"},
                                                            {"tidal_stage", "Tide Stage (ft)"},
                                                            {"pool_elevation", "Pool Elevation (ft)"},
                                                            {"water_velocity", "Water Velocity (fps)"} 
                                                        };
        private DataHandler dataHandler;

        public ChartDataRepository()
        {
            dataHandler = new DataHandler();
        }

        public ChartData Get(string stationID, string PEType)
        {
            ChartData chartData = new ChartData();
            using (var _entities = new RiverChartEntities())
            {
                DataItem dataItem = _entities.DataItems.Where(d => d.stationID.Equals(stationID) && d.PEType.Equals(PEType)).First();
                return this.Get(dataItem);
            }
        }

        public ChartData Get(DataItem dataItem)
        {

            int numberDaysOfData = Properties.Settings.Default.NumberDaysOfData; // Convert.ToInt32(WebConfigurationManager.AppSettings["NumberDaysOfData"]);
            int numberDaysOfDataObservedAndForecast = Properties.Settings.Default.NumberDaysOfDataObservedAndForecast; // Convert.ToInt32(WebConfigurationManager.AppSettings["NumberDaysOfDataObservedAndForecast"]);
            int numberDaysOfDataObservedOnly = Properties.Settings.Default.NumberDaysOfDataObservedOnly; // Convert.ToInt32(WebConfigurationManager.AppSettings["NumberDaysOfDataObservedOnly"]);

            return Get(dataItem, numberDaysOfData, numberDaysOfDataObservedAndForecast, numberDaysOfDataObservedOnly, false, null);
        }

        public ChartData Get(DataItem dataItem, int numberDaysOfData, int numberDaysOfDataObservedAndForecast, int numberDaysOfDataObservedOnly, bool getSingleDayHistoricalData, DateTime? startDateTime)
        {      
            ChartData chartData = new ChartData();
            //DateTime startDateTime;
            //if (startDateTimeValue.HasValue)
            //    startDateTime = startDateTimeValue.Value;
            //else
            //    startDateTime = DateTime.Now;

            if (dataItem != null)
            {
                string baseUrl = Properties.Settings.Default.DataCenterUrl; //ConfigurationManager.AppSettings["DataCenterUrl"].ToString();
                //int numberDaysOfData = Properties.Settings.Default.NumberDaysOfData; // Convert.ToInt32(WebConfigurationManager.AppSettings["NumberDaysOfData"]);
                //int numberDaysOfDataObservedAndForecast = Properties.Settings.Default.NumberDaysOfDataObservedAndForecast; // Convert.ToInt32(WebConfigurationManager.AppSettings["NumberDaysOfDataObservedAndForecast"]);
                //int numberDaysOfDataObservedOnly = Properties.Settings.Default.NumberDaysOfDataObservedOnly; // Convert.ToInt32(WebConfigurationManager.AppSettings["NumberDaysOfDataObservedOnly"]);

                // get data
                string urlString = string.Format("{0}?id={1}&pe={2}&dtype=b&numdays={3}", baseUrl, dataItem.stationID.Trim(), dataItem.PEType, numberDaysOfData);
                XElement x = dataHandler.GetXML(urlString);

                log.Info("urlString in ChartData Get " + urlString);

                if (x == null)
                {
                    log.Error("No data returned from data source stationID " + dataItem.stationID.Trim() + " and PEType " + dataItem.PEType + ".  UrlString was: " + urlString);
                    saveErrorToDataErrorLog(dataItem.stationID, dataItem.PEType, 1, "No data returned from data source stationID " + dataItem.stationID.Trim() + " and PEType " + dataItem.PEType + ".  UrlString was: " + urlString);
                    chartData.DataRetrievedSuccessfully = false;
                    chartData.ErrorMessage = "Source data were not available";
                    return chartData;
                    //throw new NoReturnedXMLException(dataItem.stationID);
                }

                // get observed and forecast data
                IEnumerable<XElement> observedData = dataHandler.GetValues(x, DataHandler.ValueType.observed);
                if (observedData == null || observedData.Count() == 0)
                {
                    log.Error("No observed or forecast data for stationID " + dataItem.stationID.Trim() + " and PEType " + dataItem.PEType);
                    saveErrorToDataErrorLog(dataItem.stationID, dataItem.PEType, 2, "No observed or forecast data for stationID " + dataItem.stationID.Trim() + " and PEType " + dataItem.PEType);
                    chartData.DataRetrievedSuccessfully = false;
                    chartData.ErrorMessage = "No observed or forecast data were available";
                    return chartData;
                    //throw new NoObservedDataException(dataItem.stationID);
                }

                IEnumerable<XElement> forecastData = dataHandler.GetValues(x, DataHandler.ValueType.forecast);

                bool isStandardTime;
                bool isStandardTimeF;
                string timeLabel;
                bool isDataPointCurrent = true;
                int numDays;
                DataPoint lastDataPoint;
                TimeSpan timeDiffSpan;
                List<DataPoint> observedPETypeData;
                List<DataPoint> forecastPETypeData;

                chartData.UTCOffset = dataItem.StationInformation.UTCTimeOffset;

                // set seriesValue strings and seriesValue objects
                switch (dataItem.PEType)
                {
                    case "HG":
                        if (dataItem.hasDischargeData == true && dataItem.hasStageData == true)
                        {
                            List<DataPoint> observedDischargeData = new List<DataPoint>();
                            List<DataPoint> observedStageData = new List<DataPoint>();
                            List<DataPoint> forecastDischargeData = new List<DataPoint>();
                            List<DataPoint> forecastStageData = new List<DataPoint>();
                            if(!getSingleDayHistoricalData) 
                                dataHandler.GetSeriesData(forecastData, forecastDischargeData, "discharge", forecastStageData, "stage", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTimeF, numberDaysOfDataObservedAndForecast);

                            numDays = numberDaysOfDataObservedAndForecast;
                            if (forecastDischargeData.Count == 0)
                                numDays = numberDaysOfDataObservedOnly;

                            if(!getSingleDayHistoricalData)
                                dataHandler.GetSeriesData(observedData, observedDischargeData, "discharge", observedStageData, "stage", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTime, -numDays);
                            else
                                dataHandler.GetSeriesDataForDay(observedData, observedDischargeData, "discharge", observedStageData, "stage", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTime, startDateTime);

                            // discharge is primary value if item has both discharge and stage data
                            if (observedDischargeData != null && observedDischargeData.Count > 0)
                            {
                                dataItem.primaryDataValue = observedDischargeData.Last().YValue;
                                dataItem.dataDateTime = Convert.ToDateTime(observedDischargeData.Last().XValue).ToUniversalTime();
                            }
                            else
                            {
                                dataItem.primaryDataValue = null;
                                dataItem.dataDateTime = null;
                                logSpecialProcessingMessage.Info(string.Format("Expected discharge data for {0} but none were returned", dataItem.stationID.Trim()));
                                saveErrorToDataErrorLog(dataItem.stationID, dataItem.PEType, 3, string.Format("Expected discharge data for {0} but none were returned", dataItem.stationID.Trim()));

                            }
                            //dataItem.dataDateTime = new DateTime(Convert.ToInt64(observedDischargeData.Last().XValue));
                            // stage data
                            if (observedStageData != null && observedStageData.Count > 0)
                            {
                                dataItem.secondaryDataValue = observedStageData.Last().YValue;
                                if (dataItem.dataDateTime == null)
                                    dataItem.dataDateTime = Convert.ToDateTime(observedStageData.Last().XValue).ToUniversalTime();
                            }
                            else
                            {
                                dataItem.secondaryDataValue = null;
                                logSpecialProcessingMessage.Info(string.Format("Expected stage data for {0} but none were returned", dataItem.stationID.Trim()));
                                saveErrorToDataErrorLog(dataItem.stationID, dataItem.PEType, 3, string.Format("Expected stage data for {0} but none were returned", dataItem.stationID.Trim()));
                            }
                            timeLabel = DaylightSavingsTimeChecker.GetTimeZoneLabelFromUTCOffset(dataItem.StationInformation.UTCTimeOffset, isStandardTime);
                            dataItem.dataDateTimeLabel = timeLabel;
                            if (observedDischargeData != null && observedDischargeData.Count > 0)
                            {
                                lastDataPoint = observedDischargeData.Last();// chart.Series[0].Points.Last();
                                if (!getSingleDayHistoricalData)
                                {
                                    isDataPointCurrent = IsDataPointCurrent(lastDataPoint, dataItem.StationInformation.UTCTimeOffset, isStandardTime, out timeDiffSpan);
                                    if (!isDataPointCurrent)
                                        logSpecialProcessingMessage.Info(dataItem.stationID.Trim() + " discharge out of date by " + timeDiffSpan.ToString());
                                }
                            }
                            if (observedStageData != null && observedStageData.Count > 0)
                            {
                                lastDataPoint = observedStageData.Last();
                                if (!getSingleDayHistoricalData)
                                {
                                    isDataPointCurrent = IsDataPointCurrent(lastDataPoint, dataItem.StationInformation.UTCTimeOffset, isStandardTime, out timeDiffSpan);
                                    if (!isDataPointCurrent)
                                        logSpecialProcessingMessage.Info(dataItem.stationID.Trim() + " stage out of date by " + timeDiffSpan.ToString());
                                }
                            }
                            if (observedStageData.Count > 0 && observedDischargeData.Count > 0)
                            {
                                chartData.DataRetrievedSuccessfully = true;
                            }
                            else
                            {
                                chartData.DataRetrievedSuccessfully = false;
                            }
                            chartData.ForecastUnit1Data = forecastDischargeData;
                            chartData.ObservedUnit1Data = observedDischargeData;
                            chartData.ForecastUnit2Data = forecastStageData;
                            chartData.ObservedUnit2Data = observedStageData;
                            chartData.YAxisLabelUnit1 = yAxisLabels["discharge"].ToString();
                            chartData.YAxisLabelUnit2 = yAxisLabels["stage"].ToString();
                            chartData.TimeLabel = timeLabel;
                        }
                        else if (dataItem.hasDischargeData == true)
                        {
                            List<DataPoint> observedDischargeData = new List<DataPoint>();
                            List<DataPoint> forecastDischargeData = new List<DataPoint>();

                            // get forecast values
                            if(!getSingleDayHistoricalData)
                                dataHandler.GetSeriesData(forecastData, forecastDischargeData, "discharge", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTimeF, numberDaysOfDataObservedAndForecast);

                            numDays = numberDaysOfDataObservedAndForecast;
                            if (forecastDischargeData.Count == 0)
                                numDays = numberDaysOfDataObservedOnly;
                            if(!getSingleDayHistoricalData)
                                dataHandler.GetSeriesData(observedData, observedDischargeData, "discharge", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTime, -numDays);
                            else
                                dataHandler.GetSeriesDataForDay(observedData, observedDischargeData, "discharge", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTime, startDateTime);

                            if (observedDischargeData.Count > 0)
                            {
                                dataItem.primaryDataValue = observedDischargeData.Last().YValue;
                                dataItem.dataDateTime = Convert.ToDateTime(observedDischargeData.Last().XValue).ToUniversalTime();

                                lastDataPoint = observedDischargeData.Last();
                                if (!getSingleDayHistoricalData)
                                {
                                    isDataPointCurrent = IsDataPointCurrent(lastDataPoint, dataItem.StationInformation.UTCTimeOffset, isStandardTime, out timeDiffSpan);
                                    if (!isDataPointCurrent)
                                        logSpecialProcessingMessage.Info(dataItem.stationID.Trim() + " discharge out of date by " + timeDiffSpan.ToString());
                                }
                                chartData.DataRetrievedSuccessfully = true;
                            }
                            else
                                chartData.DataRetrievedSuccessfully = false;
                            //dataItem.dataDateTime = new DateTime(Convert.ToInt64(observedDischargeData.Last().XValue));
                            timeLabel = DaylightSavingsTimeChecker.GetTimeZoneLabelFromUTCOffset(dataItem.StationInformation.UTCTimeOffset, isStandardTime);
                            dataItem.dataDateTimeLabel = timeLabel;

                            chartData.ForecastUnit1Data = forecastDischargeData;
                            chartData.ObservedUnit1Data = observedDischargeData;
                            chartData.YAxisLabelUnit1 = yAxisLabels["discharge"].ToString();
                            chartData.TimeLabel = timeLabel;
                            //CreateChart(chart, yAxisLabels["discharge"].ToString(), dataItem.stationID + ChartTypeFileNameExtensions._discharge.ToString(), timeLabel);
                        }
                        else if (dataItem.hasStageData == true)
                        {
                            //Chart chart = new Chart();

                            //chart.Series.Add(new Series("observedValues"));
                            //chart.Series.Add(new Series("forecastValues"));

                            List<DataPoint> observedStageData = new List<DataPoint>();
                            List<DataPoint> forecastStageData = new List<DataPoint>();

                            // get forecast values
                            if(!getSingleDayHistoricalData)
                                dataHandler.GetSeriesData(forecastData, forecastStageData, "stage", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTimeF, numberDaysOfDataObservedAndForecast);

                            numDays = numberDaysOfDataObservedAndForecast;
                            if (forecastStageData.Count == 0)
                                numDays = numberDaysOfDataObservedOnly;
                            if(!getSingleDayHistoricalData)
                                dataHandler.GetSeriesData(observedData, observedStageData, "stage", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTime, -numDays);
                            else
                                dataHandler.GetSeriesDataForDay(observedData, observedStageData, "stage", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTime, startDateTime);

                            if (observedStageData.Count > 0)
                            {
                                dataItem.primaryDataValue = observedStageData.Last().YValue;
                                dataItem.dataDateTime = Convert.ToDateTime(observedStageData.Last().XValue).ToUniversalTime();
                                lastDataPoint = observedStageData.Last();
                                if (!getSingleDayHistoricalData)
                                {
                                    isDataPointCurrent = IsDataPointCurrent(lastDataPoint, dataItem.StationInformation.UTCTimeOffset, isStandardTime, out timeDiffSpan);
                                    if (!isDataPointCurrent)
                                        logSpecialProcessingMessage.Info(dataItem.stationID.Trim() + " stage out of date by " + timeDiffSpan.ToString());
                                }
                                chartData.DataRetrievedSuccessfully = true;
                            }
                            else
                                chartData.DataRetrievedSuccessfully = false;
                            //dataItem.dataDateTime = new DateTime(Convert.ToInt64(observedStageData.Last().XValue));
                            timeLabel = DaylightSavingsTimeChecker.GetTimeZoneLabelFromUTCOffset(dataItem.StationInformation.UTCTimeOffset, isStandardTime);
                            dataItem.dataDateTimeLabel = timeLabel;

                            chartData.ForecastUnit1Data = forecastStageData;
                            chartData.ObservedUnit1Data = observedStageData;
                            chartData.YAxisLabelUnit1 = yAxisLabels["stage"].ToString();
                            chartData.TimeLabel = timeLabel;
                            //CreateChart(chart, yAxisLabels["stage"].ToString(), dataItem.stationID + ChartTypeFileNameExtensions._stage.ToString(), timeLabel);
                        }
                        break;
                    case "QR":
                        //chart.Series.Add(new Series("observedValues"));
                        //chart.Series.Add(new Series("forecastValues"));
                        observedPETypeData = new List<DataPoint>();
                        forecastPETypeData = new List<DataPoint>();

                        // get forecast values
                        if(!getSingleDayHistoricalData)
                            dataHandler.GetSeriesData(forecastData, forecastPETypeData, "discharge", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTimeF, numberDaysOfDataObservedAndForecast);
                        numDays = numberDaysOfDataObservedAndForecast;
                        if (forecastPETypeData.Count == 0)
                            numDays = numberDaysOfDataObservedOnly;
                        if(!getSingleDayHistoricalData)
                            dataHandler.GetSeriesData(observedData, observedPETypeData, "discharge", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTime, -numDays);
                        else
                            dataHandler.GetSeriesDataForDay(observedData, observedPETypeData, "discharge", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTime, startDateTime);

                        if (observedPETypeData.Count > 0)
                        {
                            dataItem.primaryDataValue = observedPETypeData.Last().YValue;
                            dataItem.dataDateTime = Convert.ToDateTime(observedPETypeData.Last().XValue).ToUniversalTime();
                            lastDataPoint = observedPETypeData.Last();
                            if (!getSingleDayHistoricalData)
                            {
                                isDataPointCurrent = IsDataPointCurrent(lastDataPoint, dataItem.StationInformation.UTCTimeOffset, isStandardTime, out timeDiffSpan);
                                if (!isDataPointCurrent)
                                    logSpecialProcessingMessage.Info(dataItem.stationID.Trim() + " forebay_elevation out of date by " + timeDiffSpan.ToString());
                            }
                            chartData.DataRetrievedSuccessfully = true;
                        }
                        else
                            chartData.DataRetrievedSuccessfully = false;

                        //dataItem.dataDateTime = new DateTime(Convert.ToInt64(observedPETypeData.Last().XValue));
                        timeLabel = DaylightSavingsTimeChecker.GetTimeZoneLabelFromUTCOffset(dataItem.StationInformation.UTCTimeOffset, isStandardTime);
                        dataItem.dataDateTimeLabel = timeLabel;

                        chartData.ForecastUnit1Data = forecastPETypeData;
                        chartData.ObservedUnit1Data = observedPETypeData;
                        chartData.YAxisLabelUnit1 = yAxisLabels["discharge"].ToString();
                        chartData.TimeLabel = timeLabel;
                        //CreateChart(chart, yAxisLabels["discharge"].ToString(), dataItem.stationID + ChartTypeFileNameExtensions._stage.ToString(), timeLabel);
                        break;
                    case "HP":
                        observedPETypeData = new List<DataPoint>();
                        forecastPETypeData = new List<DataPoint>();
                        // get forecast values
                        if(!getSingleDayHistoricalData)
                            dataHandler.GetSeriesData(forecastData, forecastPETypeData, "forebay_elevation", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTimeF, numberDaysOfDataObservedAndForecast);
                        numDays = numberDaysOfDataObservedAndForecast;
                        if (forecastPETypeData.Count == 0)
                            numDays = numberDaysOfDataObservedOnly;
                        if(!getSingleDayHistoricalData)
                            dataHandler.GetSeriesData(observedData, observedPETypeData, "forebay_elevation", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTime, -numDays);
                        else
                            dataHandler.GetSeriesDataForDay(observedData, observedPETypeData, "forebay_elevation", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTime, startDateTime);

                        if (observedPETypeData.Count > 0)
                        {
                            dataItem.primaryDataValue = observedPETypeData.Last().YValue;
                            dataItem.dataDateTime = Convert.ToDateTime(observedPETypeData.Last().XValue).ToUniversalTime();
                            lastDataPoint = observedPETypeData.Last();
                            if (!getSingleDayHistoricalData)
                            {
                                isDataPointCurrent = IsDataPointCurrent(lastDataPoint, dataItem.StationInformation.UTCTimeOffset, isStandardTime, out timeDiffSpan);
                                if (!isDataPointCurrent)
                                    logSpecialProcessingMessage.Info(dataItem.stationID.Trim() + " forebay_elevation out of date by " + timeDiffSpan.ToString());
                            }
                            chartData.DataRetrievedSuccessfully = true;
                        }
                        else
                            chartData.DataRetrievedSuccessfully = false;
                        //dataItem.dataDateTime = new DateTime(Convert.ToInt64(observedPETypeData.Last().XValue));
                        timeLabel = DaylightSavingsTimeChecker.GetTimeZoneLabelFromUTCOffset(dataItem.StationInformation.UTCTimeOffset, isStandardTime);
                        dataItem.dataDateTimeLabel = timeLabel;

                        chartData.ForecastUnit1Data = forecastPETypeData;
                        chartData.ObservedUnit1Data = observedPETypeData;
                        chartData.YAxisLabelUnit1 = yAxisLabels["pool_elevation"].ToString();
                        chartData.TimeLabel = timeLabel;
                        //CreateChart(chart, yAxisLabels["pool_elevation"].ToString(), dataItem.stationID + ChartTypeFileNameExtensions._elevation.ToString(), timeLabel);
                        break;
                    case "HF":
                        observedPETypeData = new List<DataPoint>();
                        forecastPETypeData = new List<DataPoint>();
                        // get forecast values
                        if(!getSingleDayHistoricalData)
                            dataHandler.GetSeriesData(forecastData, forecastPETypeData, "forebay_elevation", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTimeF, numberDaysOfDataObservedAndForecast);
                        numDays = numberDaysOfDataObservedAndForecast;
                        if (forecastPETypeData.Count == 0)
                            numDays = numberDaysOfDataObservedOnly;
                        if(!getSingleDayHistoricalData)
                            dataHandler.GetSeriesData(observedData, observedPETypeData, "forebay_elevation", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTime, -numDays);
                        else
                            dataHandler.GetSeriesDataForDay(observedData, observedPETypeData, "forebay_elevation", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTime, startDateTime);

                        if (observedPETypeData.Count > 0)
                        {
                            dataItem.primaryDataValue = observedPETypeData.Last().YValue;
                            dataItem.dataDateTime = Convert.ToDateTime(observedPETypeData.Last().XValue).ToUniversalTime();
                            lastDataPoint = observedPETypeData.Last();
                            if (!getSingleDayHistoricalData)
                            {
                                isDataPointCurrent = IsDataPointCurrent(lastDataPoint, dataItem.StationInformation.UTCTimeOffset, isStandardTime, out timeDiffSpan);
                                if (!isDataPointCurrent)
                                    logSpecialProcessingMessage.Info(dataItem.stationID.Trim() + " forebay_elevation out of date by " + timeDiffSpan.ToString());
                            }
                            chartData.DataRetrievedSuccessfully = true;
                        }
                        else
                            chartData.DataRetrievedSuccessfully = false;
                        //dataItem.dataDateTime = new DateTime(Convert.ToInt64(observedPETypeData.Last().XValue));
                        timeLabel = DaylightSavingsTimeChecker.GetTimeZoneLabelFromUTCOffset(dataItem.StationInformation.UTCTimeOffset, isStandardTime);
                        dataItem.dataDateTimeLabel = timeLabel;

                        chartData.ForecastUnit1Data = forecastPETypeData;
                        chartData.ObservedUnit1Data = observedPETypeData;
                        chartData.YAxisLabelUnit1 = yAxisLabels["forebay_elevation"].ToString();
                        chartData.TimeLabel = timeLabel;
                        //CreateChart(chart, yAxisLabels["forebay_elevation"].ToString(), dataItem.stationID + ChartTypeFileNameExtensions._elevation.ToString(), timeLabel);
                        break;
                    case "QI":
                        observedPETypeData = new List<DataPoint>();
                        forecastPETypeData = new List<DataPoint>();
                        // get forecast values
                        if(!getSingleDayHistoricalData)
                            dataHandler.GetSeriesData(forecastData, forecastPETypeData, "inflow", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTimeF, numberDaysOfDataObservedAndForecast);
                        numDays = numberDaysOfDataObservedAndForecast;
                        if (forecastPETypeData.Count == 0)
                            numDays = numberDaysOfDataObservedOnly;
                        if(!getSingleDayHistoricalData)
                            dataHandler.GetSeriesData(observedData, observedPETypeData, "inflow", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTime, -numDays);
                        else
                            dataHandler.GetSeriesDataForDay(observedData, observedPETypeData, "inflow", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTime, startDateTime);

                        if (observedPETypeData.Count > 0)
                        {
                            dataItem.primaryDataValue = observedPETypeData.Last().YValue;
                            dataItem.dataDateTime = Convert.ToDateTime(observedPETypeData.Last().XValue).ToUniversalTime();
                            lastDataPoint = observedPETypeData.Last();
                            {
                                isDataPointCurrent = IsDataPointCurrent(lastDataPoint, dataItem.StationInformation.UTCTimeOffset, isStandardTime, out timeDiffSpan);
                                if (!isDataPointCurrent)
                                    logSpecialProcessingMessage.Info(dataItem.stationID.Trim() + " inflow out of date by " + timeDiffSpan.ToString());
                            }
                            chartData.DataRetrievedSuccessfully = true;
                        }
                        else
                            chartData.DataRetrievedSuccessfully = false;
                        //dataItem.dataDateTime = new DateTime(Convert.ToInt64(observedPETypeData.Last().XValue));
                        timeLabel = DaylightSavingsTimeChecker.GetTimeZoneLabelFromUTCOffset(dataItem.StationInformation.UTCTimeOffset, isStandardTime);
                        dataItem.dataDateTimeLabel = timeLabel;

                        chartData.ForecastUnit1Data = forecastPETypeData;
                        chartData.ObservedUnit1Data = observedPETypeData;
                        chartData.YAxisLabelUnit1 = yAxisLabels["inflow"].ToString();
                        chartData.TimeLabel = timeLabel;
                        //CreateChart(chart, yAxisLabels["inflow"].ToString(), dataItem.stationID + ChartTypeFileNameExtensions._inflow.ToString(), timeLabel);
                        break;
                    case "HM":
                        observedPETypeData = new List<DataPoint>();
                        forecastPETypeData = new List<DataPoint>();
                        // get forecast values
                        if(!getSingleDayHistoricalData)
                            dataHandler.GetSeriesData(forecastData, forecastPETypeData, "tidal_stage", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTimeF, numberDaysOfDataObservedAndForecast);
                        numDays = numberDaysOfDataObservedAndForecast;
                        if (forecastPETypeData.Count == 0)
                            numDays = numberDaysOfDataObservedOnly;
                        if(!getSingleDayHistoricalData)
                            dataHandler.GetSeriesData(observedData, observedPETypeData, "tidal_stage", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTime, -numDays);
                        else
                            dataHandler.GetSeriesDataForDay(observedData, observedPETypeData, "tidal_stage", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTime, startDateTime);

                        if (observedPETypeData.Count > 0)
                        {
                            dataItem.primaryDataValue = observedPETypeData.Last().YValue;
                            dataItem.dataDateTime = Convert.ToDateTime(observedPETypeData.Last().XValue).ToUniversalTime();
                            lastDataPoint = observedPETypeData.Last();
                            if (!getSingleDayHistoricalData)
                            {
                                isDataPointCurrent = IsDataPointCurrent(lastDataPoint, dataItem.StationInformation.UTCTimeOffset, isStandardTime, out timeDiffSpan);
                                if (!isDataPointCurrent)
                                    logSpecialProcessingMessage.Info(dataItem.stationID.Trim() + " tidal_stage out of date by " + timeDiffSpan.ToString());
                            }
                            chartData.DataRetrievedSuccessfully = true;
                        }
                        else
                            chartData.DataRetrievedSuccessfully = false;
                        //dataItem.dataDateTime = new DateTime(Convert.ToInt64(observedPETypeData.Last().XValue));
                        timeLabel = DaylightSavingsTimeChecker.GetTimeZoneLabelFromUTCOffset(dataItem.StationInformation.UTCTimeOffset, isStandardTime);
                        dataItem.dataDateTimeLabel = timeLabel;

                        chartData.ForecastUnit1Data = forecastPETypeData;
                        chartData.ObservedUnit1Data = observedPETypeData;
                        chartData.YAxisLabelUnit1 = yAxisLabels["tidal_stage"].ToString();
                        chartData.TimeLabel = timeLabel;
                        //CreateChart(chart, yAxisLabels["tidal_stage"].ToString(), dataItem.stationID + ChartTypeFileNameExtensions._tide.ToString(), timeLabel);
                        break;
                    case "WV":
                        observedPETypeData = new List<DataPoint>();
                        forecastPETypeData = new List<DataPoint>();
                        // get forecast values
                        if (!getSingleDayHistoricalData)
                            dataHandler.GetSeriesData(forecastData, forecastPETypeData, "water_velocity", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTimeF, numberDaysOfDataObservedAndForecast);
                        numDays = numberDaysOfDataObservedAndForecast;
                        if (forecastPETypeData.Count == 0)
                            numDays = numberDaysOfDataObservedOnly;
                        if (!getSingleDayHistoricalData)
                            dataHandler.GetSeriesData(observedData, observedPETypeData, "water_velocity", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTime, -numDays);
                        else
                            dataHandler.GetSeriesDataForDay(observedData, observedPETypeData, "water_velocity", dataItem.StationInformation.UTCTimeOffset, dataItem.StationInformation.observesDaylightTime, out isStandardTime, startDateTime);

                        if (observedPETypeData.Count > 0)
                        {
                            dataItem.primaryDataValue = observedPETypeData.Last().YValue;
                            dataItem.dataDateTime = Convert.ToDateTime(observedPETypeData.Last().XValue).ToUniversalTime();
                            lastDataPoint = observedPETypeData.Last();
                            if (!getSingleDayHistoricalData)
                            {
                                isDataPointCurrent = IsDataPointCurrent(lastDataPoint, dataItem.StationInformation.UTCTimeOffset, isStandardTime, out timeDiffSpan);
                                if (!isDataPointCurrent)
                                    logSpecialProcessingMessage.Info(dataItem.stationID.Trim() + " water_velocity out of date by " + timeDiffSpan.ToString());
                            }
                            chartData.DataRetrievedSuccessfully = true;
                        }
                        else
                            chartData.DataRetrievedSuccessfully = false;
                        //dataItem.dataDateTime = new DateTime(Convert.ToInt64(observedPETypeData.Last().XValue));
                        timeLabel = DaylightSavingsTimeChecker.GetTimeZoneLabelFromUTCOffset(dataItem.StationInformation.UTCTimeOffset, isStandardTime);
                        dataItem.dataDateTimeLabel = timeLabel;

                        chartData.ForecastUnit1Data = forecastPETypeData;
                        chartData.ObservedUnit1Data = observedPETypeData;
                        chartData.YAxisLabelUnit1 = yAxisLabels["water_velocity"].ToString();
                        chartData.TimeLabel = timeLabel;
                        //CreateChart(chart, yAxisLabels["tidal_stage"].ToString(), dataItem.stationID + ChartTypeFileNameExtensions._tide.ToString(), timeLabel);
                        break;

                    default:
                        log.Error("Tried to create chart data for the PEType " + dataItem.PEType);
                        saveErrorToDataErrorLog(dataItem.stationID, dataItem.PEType, 4, "Tried to create chart data for the stationID " + dataItem.stationID.Trim() + " and PEType " + dataItem.PEType);
                        chartData.DataRetrievedSuccessfully = false;
                        chartData.ErrorMessage = "Data are not available for this type of measurement";
                        break;
                }
                return chartData;
            }
            else // no item found in database
            {
                log.Error("Could not find data for stationID " + dataItem.stationID.Trim() + " and PEType " + dataItem.PEType);
                saveErrorToDataErrorLog(dataItem.stationID, dataItem.PEType, 5, "Could not find data for stationID " + dataItem.stationID.Trim() + " and PEType " + dataItem.PEType);
                chartData.DataRetrievedSuccessfully = false;
                chartData.ErrorMessage = "Data are no longer available for this station";
                return chartData;
            }
        }
        

        private bool IsDataPointCurrent(DataPoint dataPoint, int utcTimeOffset, bool isStandardTime, out TimeSpan timeDiffSpan)
        {
            DateTime timeLastObservedValue = Convert.ToDateTime(dataPoint.XValue).ToUniversalTime();
            //DateTime timeLastObservedValue = new DateTime(Convert.ToInt64(dataPoint.XValue));
            DateTime timeNow = DateTime.UtcNow;
            timeNow = timeNow.AddHours((double)utcTimeOffset);
            if (!isStandardTime)
                timeNow = timeNow.AddHours(1.0);
            //timeDiffSpan = DateTime.UtcNow.Subtract(timeLastObservedValue.ToUniversalTime());
            timeDiffSpan = timeNow.Subtract(timeLastObservedValue);
            if (timeDiffSpan > new TimeSpan(6, 0, 0))
                return false;
            else
                return true;
        }

        private void saveErrorToDataErrorLog(string stationId, string PEType, int errorCode, string message)
        {
            ChartData chartData = new ChartData();
            using (var _entities = new RiverChartEntities())
            {
                DataErrorLog dataErrorLog = new DataErrorLog();
                dataErrorLog.stationID = stationId;
                dataErrorLog.PEType = PEType;
                dataErrorLog.errorTypeId = errorCode;
                dataErrorLog.errorMessage = message;
                _entities.DataErrorLogs.Add(dataErrorLog);
                _entities.SaveChanges();
            }
        }
    }

    //[Serializable]
    //public class NoReturnedXMLException : System.Exception
    //{
    //    public NoReturnedXMLException() { }
    //    public NoReturnedXMLException(string message) : base(message) { }
    //    protected NoReturnedXMLException(SerializationInfo info,
    //       StreamingContext context)
    //        : base(info, context) { }
    //}
    //[Serializable]
    //public class NoObservedDataException : System.Exception
    //{
    //    public NoObservedDataException() { }
    //    public NoObservedDataException(string message) : base(message) { }
    //    protected NoObservedDataException(SerializationInfo info,
    //       StreamingContext context)
    //        : base(info, context) { }
    //}
}