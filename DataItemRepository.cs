﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace RiverChartDataRequestAndProcessLibrary
{
    public class DataItemRepository : IDataItemRepository
    {
        #region IDataItemRepository Members

        public IEnumerable<SimpleDataItem> Get(string searchTerms, string PETypes)
        {
            //using (var context = new DataItemContext())
            //using (var context = 
            //var context = new DbContext();
            //using (var context = new DbContext())
            //var context = new RiverChartEntities();
            List<SimpleDataItem> list = new List<SimpleDataItem>();
            using (var _entities = new RiverChartEntities())
            {
                
                var outd =  _entities.vw_DataItemForSearch.Where(p => p.name.Contains(searchTerms));
                
                foreach(vw_DataItemForSearch i in outd)
                {
                    list.Add(new SimpleDataItem(i));
                }
                //return outd.ToList();
            }
            
            return list;
        }

        public IEnumerable<SimpleDataItem> Get()
        {

            List<SimpleDataItem> list = new List<SimpleDataItem>();
            using (var _entities = new RiverChartEntities())
            {

                var outd = _entities.vw_DataItemForSearch.OrderBy(v => v.name);

                foreach (vw_DataItemForSearch i in outd)
                {
                    list.Add(new SimpleDataItem(i));
                }
                //return outd.ToList();
            }

            return list;
        }

        #endregion
    }
}