﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Data;
//using System.Web.UI.DataVisualization.Charting;
//using System.Drawing;

namespace RiverChartDataRequestAndProcessLibrary
{
    class DataHandler
    {
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("RiverChartDataRequestAndProcessLibrary.GeneralMessages");

        public enum ValueType { observed, forecast };

        public XElement GetXML(string urlString)
        {
            try
            {
                return XElement.Load(urlString);
            }
            catch (Exception e)
            {
                log.Error("GetXML for url: " + urlString + "; " + e.Message);
                //throw e;
                return null;
            }
        }


        // get either forecast or observed values
        public System.Collections.Generic.IEnumerable<XElement> GetValues(XElement x, Enum valueType)
        {
            return (from c in x.Elements(x.Name.Namespace + "SiteData").Elements(x.Name.Namespace + valueType.ToString() + "Data").Elements(x.Name.Namespace + valueType.ToString() + "Value") select c);
        }


        public void GetSeriesData(IEnumerable<XElement> values, List<DataPoint> series, string seriesValueName, int utcOffset, bool observesDaylightTime, out bool isStandardTime, int numDays)
        {
            isStandardTime = true;
            bool isStandardTime2;

            if (values == null || values.Count() == 0)
                return;

            int i = 0;
            DateTime dataDateTime;

            double value;
            int valueCount = 0;
            XElement x;
            XNamespace name;
            DateTime cutoffDateTime = new DateTime();
            try
            {
                name = values.ElementAt(0).Name.Namespace;
                if (numDays < 0) // go back numDays from last observed day
                {
                    XElement xlast = values.Last();
                    DateTime lastDateTime = DaylightSavingsTimeChecker.ComputeAdjustedTime(Convert.ToDateTime(xlast.Element(name + "dataDateTime").Value).ToUniversalTime(), utcOffset, observesDaylightTime, out isStandardTime);
                    cutoffDateTime = lastDateTime.AddDays((double)numDays);
                    cutoffDateTime = new DateTime(cutoffDateTime.Year, cutoffDateTime.Month, cutoffDateTime.Day, 0, 0, 0);
                }
                if (numDays > 0) // go forward numDays from last observed day
                {
                    XElement xfirst = values.First();
                    DateTime firstDateTime = DaylightSavingsTimeChecker.ComputeAdjustedTime(Convert.ToDateTime(xfirst.Element(name + "dataDateTime").Value).ToUniversalTime(), utcOffset, observesDaylightTime, out isStandardTime);
                    cutoffDateTime = firstDateTime.AddDays((double)numDays + 1.0);
                    cutoffDateTime = new DateTime(cutoffDateTime.Year, cutoffDateTime.Month, cutoffDateTime.Day, 0, 0, 0);

                }
                // if we need to cut off from the start
                if (numDays < 0)
                {
                    while (i < values.Count() && DaylightSavingsTimeChecker.ComputeAdjustedTime(Convert.ToDateTime(values.ElementAt(i).Element(name + "dataDateTime").Value).ToUniversalTime(), utcOffset, observesDaylightTime, out isStandardTime).CompareTo(cutoffDateTime) < 0)
                    {
                        i++;
                    }
                }
                while (i < values.Count() && (numDays <= 0 || (numDays > 0 && DaylightSavingsTimeChecker.ComputeAdjustedTime(Convert.ToDateTime(values.ElementAt(i).Element(name + "dataDateTime").Value).ToUniversalTime(), utcOffset, observesDaylightTime, out isStandardTime).CompareTo(cutoffDateTime) < 0)))
                {
                    valueCount = 0;
                    // get current value
                    x = values.ElementAt(i);
                    i++;
                    value = Convert.ToDouble(x.Element(name + seriesValueName).Value);
                    valueCount++;
                    dataDateTime = DaylightSavingsTimeChecker.ComputeAdjustedTime(Convert.ToDateTime(x.Element(name + "dataDateTime").Value).ToUniversalTime(), utcOffset, observesDaylightTime, out isStandardTime);
                    // loop until time of next is different

                    while (i < values.Count() && dataDateTime == DaylightSavingsTimeChecker.ComputeAdjustedTime(Convert.ToDateTime(values.ElementAt(i).Element(name + "dataDateTime").Value).ToUniversalTime(), utcOffset, observesDaylightTime, out isStandardTime2))
                    {
                        x = values.ElementAt(i);
                        i++;
                        value += Convert.ToDouble(x.Element(name + seriesValueName).Value);
                        valueCount++;
                        dataDateTime = DaylightSavingsTimeChecker.ComputeAdjustedTime(Convert.ToDateTime(x.Element(name + "dataDateTime").Value).ToUniversalTime(), utcOffset, observesDaylightTime, out isStandardTime);
                    }

                    DataPoint dp = new DataPoint(dataDateTime.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"), value / valueCount);  // insert avg
                    //DataPoint dp = new DataPoint(dataDateTime.Ticks, value / valueCount);  // insert avg

                    series.Add(dp);
                }

            }
            catch (Exception e)
            {
                //log.Error("GetSeriesData: " + e.Message);
                throw e;
            }
        }


        public void GetSeriesData(IEnumerable<XElement> values, List<DataPoint> series, string seriesValueName, List<DataPoint> series2, string seriesValueName2, int utcOffset, bool observesDaylightTime, out bool isStandardTime, int numDays)
        {
            isStandardTime = true;

            if (values == null || values.Count() == 0)
                return;

            int i = 0;
            DateTime dataDateTime;

            double value = 0;
            double value2 = 0;

            int valueCount = 0;
            int valueCount2 = 0;
            XElement x;
            XNamespace name;
            DateTime cutoffDateTime = new DateTime();
            try
            {
                name = values.ElementAt(0).Name.Namespace;
                if (numDays < 0) // go back numDays from last observed day
                {
                    XElement xlast = values.Last();
                    DateTime lastDateTime = DaylightSavingsTimeChecker.ComputeAdjustedTime(Convert.ToDateTime(xlast.Element(name + "dataDateTime").Value).ToUniversalTime(), utcOffset, observesDaylightTime, out isStandardTime);
                    cutoffDateTime = lastDateTime.AddDays((double)numDays);
                    cutoffDateTime = new DateTime(cutoffDateTime.Year, cutoffDateTime.Month, cutoffDateTime.Day, 0, 0, 0);
                }
                if (numDays > 0) // go forward numDays from last observed day
                {
                    XElement xfirst = values.First();
                    DateTime firstDateTime = DaylightSavingsTimeChecker.ComputeAdjustedTime(Convert.ToDateTime(xfirst.Element(name + "dataDateTime").Value).ToUniversalTime(), utcOffset, observesDaylightTime, out isStandardTime);
                    cutoffDateTime = firstDateTime.AddDays((double)numDays + 1.0);
                    cutoffDateTime = new DateTime(cutoffDateTime.Year, cutoffDateTime.Month, cutoffDateTime.Day, 0, 0, 0);

                }

                // if we need to cut off from the start
                //bool lookingForCutoff = true;
                if (numDays < 0)
                {
                    //while (i < values.Count() && lookingForCutoff)
                    while (i < values.Count() && DaylightSavingsTimeChecker.ComputeAdjustedTime(Convert.ToDateTime(values.ElementAt(i).Element(name + "dataDateTime").Value).ToUniversalTime(), utcOffset, observesDaylightTime, out isStandardTime).CompareTo(cutoffDateTime) < 0)
                    {
                        //x = values.ElementAt(i);
                        i++;
                    }
                    //    dataDateTime = DaylightSavingsTimeChecker.ComputeAdjustedTime(Convert.ToDateTime(x.Element(name + "dataDateTime").Value).ToUniversalTime(), utcOffset, observesDaylightTime, out isStandardTime);
                    //    if (dataDateTime.CompareTo(cutoffDateTime) < 0)
                    //        i++;
                    //    else
                    //        lookingForCutoff = false;
                    //}
                }

                while (i < values.Count() && (numDays <= 0 || (numDays > 0 && DaylightSavingsTimeChecker.ComputeAdjustedTime(Convert.ToDateTime(values.ElementAt(i).Element(name + "dataDateTime").Value).ToUniversalTime(), utcOffset, observesDaylightTime, out isStandardTime).CompareTo(cutoffDateTime) < 0)))
                {

                    valueCount = 0;
                    valueCount2 = 0;

                    // get current value
                    x = values.ElementAt(i);
                    i++;
                    //if ((numDays > 0 && dataDateTime.CompareTo(cutoffDateTime) < 0) || numDays <= 0)
                    //{
                    if (x.Element(name + seriesValueName) != null)
                    {
                        value = Convert.ToDouble(x.Element(name + seriesValueName).Value);
                        valueCount++;
                    }
                    if (x.Element(name + seriesValueName2) != null)
                    {
                        value2 = Convert.ToDouble(x.Element(name + seriesValueName2).Value);
                        valueCount2++;
                    }
                    dataDateTime = DaylightSavingsTimeChecker.ComputeAdjustedTime(Convert.ToDateTime(x.Element(name + "dataDateTime").Value).ToUniversalTime(), utcOffset, observesDaylightTime, out isStandardTime);
                    // loop until time of next is different

                    while (i < values.Count() && dataDateTime == DaylightSavingsTimeChecker.ComputeAdjustedTime(Convert.ToDateTime(values.ElementAt(i).Element(name + "dataDateTime").Value).ToUniversalTime(), utcOffset, observesDaylightTime, out isStandardTime))
                    {
                        x = values.ElementAt(i);
                        i++;
                        if (x.Element(name + seriesValueName) != null)
                        {

                            value += Convert.ToDouble(x.Element(name + seriesValueName).Value);
                            valueCount++;
                        }
                        if (x.Element(name + seriesValueName2) != null)
                        {
                            value2 += Convert.ToDouble(x.Element(name + seriesValueName2).Value);
                            valueCount2++;
                        }
                        dataDateTime = DaylightSavingsTimeChecker.ComputeAdjustedTime(Convert.ToDateTime(x.Element(name + "dataDateTime").Value).ToUniversalTime(), utcOffset, observesDaylightTime, out isStandardTime);
                    }

                    if (valueCount > 0)
                    {
                        DataPoint dp = new DataPoint(dataDateTime.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"), value / valueCount);  // insert avg
                        //DataPoint dp = new DataPoint(dataDateTime.Ticks, value / valueCount);  // insert avg
                        series.Add(dp);
                    }
                    if (valueCount2 > 0)
                    {
                        DataPoint dp2 = new DataPoint(dataDateTime.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"), value2 / valueCount2);  // insert avg
                        //DataPoint dp2 = new DataPoint(dataDateTime.Ticks, value2 / valueCount2);  // insert avg
                        series2.Add(dp2);
                    }
                    //}
                }
            }
            catch (Exception e)
            {
                //log.Error("GetSeriesData (two series): " + e.Message);
                throw e;
            }
        }

        public void GetSeriesDataForDay(IEnumerable<XElement> values, List<DataPoint> series, string seriesValueName, List<DataPoint> series2, string seriesValueName2, int utcOffset, bool observesDaylightTime, out bool isStandardTime, DateTime? startDateTime)
        {
            isStandardTime = true;

            if (values == null || values.Count() == 0)
                return;

            int i = 0;
            DateTime dataDateTime;

            double value = 0;
            double value2 = 0;

            int valueCount = 0;
            int valueCount2 = 0;
            XElement x;
            XNamespace name;
            DateTime cutoffDateTime = new DateTime();
            try
            {
                name = values.ElementAt(0).Name.Namespace;

                cutoffDateTime = startDateTime.Value;

                // get to start of day
                while (i < values.Count() && DaylightSavingsTimeChecker.ComputeAdjustedTime(Convert.ToDateTime(values.ElementAt(i).Element(name + "dataDateTime").Value).ToUniversalTime(), utcOffset, observesDaylightTime, out isStandardTime).CompareTo(cutoffDateTime) < 0)
                {
                    i++;
                }

                DateTime currentCutoffTime = cutoffDateTime;
                currentCutoffTime = currentCutoffTime.AddHours(1);
                cutoffDateTime = cutoffDateTime.AddDays(1);
                while (i < values.Count() && DaylightSavingsTimeChecker.ComputeAdjustedTime(Convert.ToDateTime(values.ElementAt(i).Element(name + "dataDateTime").Value).ToUniversalTime(), utcOffset, observesDaylightTime, out isStandardTime).CompareTo(cutoffDateTime) < 0)
                {

                    valueCount = 0;
                    valueCount2 = 0;

                    // get current value
                    x = values.ElementAt(i);
                    i++;
                    dataDateTime = DaylightSavingsTimeChecker.ComputeAdjustedTime(Convert.ToDateTime(x.Element(name + "dataDateTime").Value).ToUniversalTime(), utcOffset, observesDaylightTime, out isStandardTime);
                    if (dataDateTime.CompareTo(currentCutoffTime) >= 0)
                    {
                        //if ((numDays > 0 && dataDateTime.CompareTo(cutoffDateTime) < 0) || numDays <= 0)
                        //{
                        if (x.Element(name + seriesValueName) != null)
                        {
                            value = Convert.ToDouble(x.Element(name + seriesValueName).Value);
                            valueCount++;
                        }
                        if (x.Element(name + seriesValueName2) != null)
                        {
                            value2 = Convert.ToDouble(x.Element(name + seriesValueName2).Value);
                            valueCount2++;
                        }
                        // loop until time of next is different

                        while (i < values.Count() && dataDateTime == DaylightSavingsTimeChecker.ComputeAdjustedTime(Convert.ToDateTime(values.ElementAt(i).Element(name + "dataDateTime").Value).ToUniversalTime(), utcOffset, observesDaylightTime, out isStandardTime))
                        {
                            x = values.ElementAt(i);
                            i++;
                            if (x.Element(name + seriesValueName) != null)
                            {

                                value += Convert.ToDouble(x.Element(name + seriesValueName).Value);
                                valueCount++;
                            }
                            if (x.Element(name + seriesValueName2) != null)
                            {
                                value2 += Convert.ToDouble(x.Element(name + seriesValueName2).Value);
                                valueCount2++;
                            }
                            dataDateTime = DaylightSavingsTimeChecker.ComputeAdjustedTime(Convert.ToDateTime(x.Element(name + "dataDateTime").Value).ToUniversalTime(), utcOffset, observesDaylightTime, out isStandardTime);
                        }

                        if (valueCount > 0)
                        {
                            DataPoint dp = new DataPoint(dataDateTime.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"), value / valueCount);  // insert avg
                            //DataPoint dp = new DataPoint(dataDateTime.Ticks, value / valueCount);  // insert avg
                            series.Add(dp);
                        }
                        if (valueCount2 > 0)
                        {
                            DataPoint dp2 = new DataPoint(dataDateTime.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"), value2 / valueCount2);  // insert avg
                            //DataPoint dp2 = new DataPoint(dataDateTime.Ticks, value2 / valueCount2);  // insert avg
                            series2.Add(dp2);
                        }
                        currentCutoffTime = currentCutoffTime.AddHours(1);
                    }
                }
            }
            catch (Exception e)
            {
                //log.Error("GetSeriesData (two series): " + e.Message);
                throw e;
            }
        }

        public void GetSeriesDataForDay(IEnumerable<XElement> values, List<DataPoint> series, string seriesValueName, int utcOffset, bool observesDaylightTime, out bool isStandardTime, DateTime? startDateTime)
        {
            isStandardTime = true;

            if (values == null || values.Count() == 0)
                return;

            int i = 0;
            DateTime dataDateTime;

            double value = 0;

            int valueCount = 0;
            XElement x;
            XNamespace name;
            DateTime cutoffDateTime = new DateTime();
            try
            {
                name = values.ElementAt(0).Name.Namespace;

                cutoffDateTime = startDateTime.Value;

                // get to start of day
                while (i < values.Count() && DaylightSavingsTimeChecker.ComputeAdjustedTime(Convert.ToDateTime(values.ElementAt(i).Element(name + "dataDateTime").Value).ToUniversalTime(), utcOffset, observesDaylightTime, out isStandardTime).CompareTo(cutoffDateTime) < 0)
                {
                    i++;
                }

                DateTime currentCutoffTime = cutoffDateTime;
                currentCutoffTime = currentCutoffTime.AddHours(1);
                cutoffDateTime = cutoffDateTime.AddDays(1);
                while (i < values.Count() && DaylightSavingsTimeChecker.ComputeAdjustedTime(Convert.ToDateTime(values.ElementAt(i).Element(name + "dataDateTime").Value).ToUniversalTime(), utcOffset, observesDaylightTime, out isStandardTime).CompareTo(cutoffDateTime) < 0)
                {
                    valueCount = 0;

                    // get current value
                    x = values.ElementAt(i);
                    i++;
                    dataDateTime = DaylightSavingsTimeChecker.ComputeAdjustedTime(Convert.ToDateTime(x.Element(name + "dataDateTime").Value).ToUniversalTime(), utcOffset, observesDaylightTime, out isStandardTime);

                    if (dataDateTime.CompareTo(currentCutoffTime) >= 0)
                    {
                        if (x.Element(name + seriesValueName) != null)
                        {
                            value = Convert.ToDouble(x.Element(name + seriesValueName).Value);
                            valueCount++;
                        }

                        // loop until time of next is different
                        while (i < values.Count() && dataDateTime == DaylightSavingsTimeChecker.ComputeAdjustedTime(Convert.ToDateTime(values.ElementAt(i).Element(name + "dataDateTime").Value).ToUniversalTime(), utcOffset, observesDaylightTime, out isStandardTime))
                        {
                            x = values.ElementAt(i);
                            i++;
                            if (x.Element(name + seriesValueName) != null)
                            {

                                value += Convert.ToDouble(x.Element(name + seriesValueName).Value);
                                valueCount++;
                            }

                            dataDateTime = DaylightSavingsTimeChecker.ComputeAdjustedTime(Convert.ToDateTime(x.Element(name + "dataDateTime").Value).ToUniversalTime(), utcOffset, observesDaylightTime, out isStandardTime);
                        }

                        if (valueCount > 0)
                        {
                            DataPoint dp = new DataPoint(dataDateTime.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"), value / valueCount);  // insert avg
                            //DataPoint dp = new DataPoint(dataDateTime.Ticks, value / valueCount);  // insert avg
                            series.Add(dp);
                        }
                        currentCutoffTime = currentCutoffTime.AddHours(1);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        //private void CreateChart(IEnumerable<XElement> observedValues, IEnumerable<XElement> forecastValues, string chartName)
        //{
        //    Chart Chart1 = new Chart();
        //    Chart1.ImageType = ChartImageType.Png;
        //    Chart1.ImageStorageMode = ImageStorageMode.UseImageLocation;
        //    Chart1.ImageLocation = "./alsea.png";

        //    Double stage;
        //    DateTime dataDateTime;
        //    Series seriesObs = Chart1.Series["observedValues"];

        //    int i = 0;

        //    double minDataValue = -999.0;
        //    double maxDataValue = -999.0;

        //    Chart1.Series[0].IsValueShownAsLabel = false;

        //    //foreach (var observedValue in observedValues)
        //    //{
        //    //    i++;
        //    //    if (observedValue.Attribute("tsCode").Value != "RGX")
        //    //    {
        //    //        stage = Convert.ToDouble(observedValue.Element(test.Name.Namespace + "stage").Value);
        //    //        if (minDataValue == -999 || minDataValue > stage)
        //    //            minDataValue = stage;
        //    //        if (maxDataValue == -999 || maxDataValue < stage)
        //    //            maxDataValue = stage;

        //    //        dataDateTime = Convert.ToDateTime(observedValue.Element(test.Name.Namespace + "dataDateTime").Value);

        //    //        DataPoint dp = new DataPoint(dataDateTime.Ticks, stage);

        //    //        seriesObs.Points.Add(dp);

        //    //    }
        //    //}

        //    Series seriesFcst = Chart1.Series["forecastValues"];
        //    DateTime currentTime = DateTime.UtcNow;
        //    DateTime endTime = new DateTime(currentTime.Year, currentTime.Month, currentTime.Day);
        //    endTime = endTime.AddDays(7);
        //    foreach (var forecastValue in forecastValues)
        //    {

        //        stage = Convert.ToDouble(forecastValue.Element(test.Name.Namespace + "stage").Value);
        //        dataDateTime = Convert.ToDateTime(forecastValue.Element(test.Name.Namespace + "dataDateTime").Value);
        //        if (minDataValue == -999 || minDataValue > stage)
        //            minDataValue = stage;
        //        if (maxDataValue == -999 || maxDataValue < stage)
        //            maxDataValue = stage;
        //        if (dataDateTime <= endTime)
        //        {
        //            DataPoint dp = new DataPoint(dataDateTime.Ticks, stage);

        //            seriesFcst.Points.Add(dp);
        //        }
        //        else
        //            break;

        //    }


        //    Chart1.ChartAreas[0].AxisY.Minimum = Math.Floor(minDataValue);
        //    Chart1.ChartAreas[0].AxisY.Maximum = Math.Ceiling(maxDataValue);

        //    Chart1.ChartAreas[0].AxisY.Title = "River Stage (ft)";
        //    Chart1.ChartAreas[0].AxisY.TitleFont = new Font("Helvetica", 10);

        //    VerticalLineAnnotation currentTimeMarker = new VerticalLineAnnotation();
        //    currentTimeMarker.AxisX = Chart1.ChartAreas[0].AxisX;
        //    currentTimeMarker.AxisY = Chart1.ChartAreas[0].AxisY;
        //    currentTimeMarker.X = currentTime.Ticks;
        //    currentTimeMarker.Y = Math.Floor(minDataValue);
        //    currentTimeMarker.Height = Math.Ceiling(maxDataValue) - Math.Floor(minDataValue);
        //    currentTimeMarker.LineColor = Color.Red;
        //    currentTimeMarker.LineWidth = 2;
        //    currentTimeMarker.IsSizeAlwaysRelative = false;

        //    Chart1.Annotations.Add(currentTimeMarker);

        //    TimeSpan halfDay = new TimeSpan(12, 0, 0);

        //    Chart1.ChartAreas[0].AxisX2.MajorGrid.Enabled = false;
        //    Chart1.ChartAreas[0].AxisX2.MajorTickMark.Enabled = true;
        //    Chart1.ChartAreas[0].AxisX2.Enabled = AxisEnabled.True;
        //    Chart1.ChartAreas[0].AxisX2.LabelStyle.Font = new Font("Helvetica", 9);
        //    Chart1.ChartAreas[0].AxisX2.LabelStyle.ForeColor = Color.Red;
        //    Chart1.ChartAreas[0].AxisX2.MajorTickMark.LineColor = Color.Red;
        //    CustomLabel currentTimeLabel = Chart1.ChartAreas[0].AxisX2.CustomLabels.Add(currentTime.Ticks - 6 * halfDay.Ticks, currentTime.Ticks + 6 * halfDay.Ticks, "Current Time");
        //    currentTimeLabel.GridTicks = GridTickTypes.TickMark;


        //    DateTime minDateTime = new DateTime(Convert.ToInt64(seriesObs.Points[0].XValue));
        //    minDateTime = new DateTime(minDateTime.Year, minDateTime.Month, minDateTime.Day);

        //    DateTime maxDateTime = new DateTime(Convert.ToInt64(seriesFcst.Points[seriesFcst.Points.Count - 1].XValue));
        //    maxDateTime = new DateTime(maxDateTime.Year, maxDateTime.Month, maxDateTime.Day);
        //    maxDateTime = maxDateTime.AddDays(1.0);
        //    TimeSpan plotTs = maxDateTime.Subtract(minDateTime);

        //    // set x axis properties
        //    Chart1.ChartAreas[0].AxisX.LabelStyle.Font = new Font("Helvetica", 8);
        //    Chart1.ChartAreas[0].AxisX.LabelStyle.IsStaggered = true;

        //    CustomLabel element;

        //    Chart1.ChartAreas[0].AxisX.Minimum = minDateTime.Ticks;
        //    Chart1.ChartAreas[0].AxisX.Maximum = maxDateTime.Ticks;
        //    Chart1.ChartAreas[0].AxisX2.Minimum = minDateTime.Ticks;
        //    Chart1.ChartAreas[0].AxisX2.Maximum = maxDateTime.Ticks;

        //    for (int j = 0; j <= plotTs.Days; j++)
        //    {
        //        //element = Chart1.ChartAreas[0].AxisX.CustomLabels.Add(minDateTime.Ticks - halfDay.Ticks, minDateTime.Ticks + halfDay.Ticks, minDateTime.ToShortDateString());
        //        element = Chart1.ChartAreas[0].AxisX.CustomLabels.Add(minDateTime.Ticks - halfDay.Ticks, minDateTime.Ticks + halfDay.Ticks, minDateTime.Month.ToString() + "//" + minDateTime.Day.ToString());
        //        element.GridTicks = GridTickTypes.All;

        //        minDateTime = minDateTime.AddDays(1.0);
        //    }




        //}

    }
}
