//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RiverChartDataRequestAndProcessLibrary
{
    using System;
    using System.Collections.Generic;
    
    public partial class DataItemObservation
    {
        public string stationID { get; set; }
        public string PEType { get; set; }
        public bool isStageData { get; set; }
        public bool isDischargeData { get; set; }
        public string timeAsString { get; set; }
        public System.DateTime time { get; set; }
        public double value { get; set; }
        public int DataItemObservationId { get; set; }
    }
}
