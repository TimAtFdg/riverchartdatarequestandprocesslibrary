﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiverChartDataRequestAndProcessLibrary
{
    public static class GetChartDataFromObservedData
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger("RiverChartDataRequestAndProcessLibrary.GeneralMessages");

        private static Hashtable yAxisLabels = new Hashtable() { 
                                                            {"stage", "River Stage (ft)"},
                                                            {"discharge", "Discharge (cfs)"},
                                                            {"inflow", "Inflow (cfs)"},
                                                            {"forebay_elevation", "Forebay Elevation (ft)"},
                                                            {"tidal_stage", "Tide Stage (ft)"},
                                                            {"pool_elevation", "Pool Elevation (ft)"}
                                                        };

        public static ChartData GetForDates(string stationId, string peType, DateTime startDate, DateTime endDate, DateTime logDate)
        {
            ChartData data = new ChartData();
            bool isStandardTime = DaylightSavingsTimeChecker.IsStandardTime(logDate);

            using (var _entities = new RiverChartEntities())
            {
                DataItem item = _entities.DataItems.Where(q => q.stationID == stationId && q.PEType == peType).FirstOrDefault();
                if (item == null)
                    return null;

                data.TimeLabel = DaylightSavingsTimeChecker.GetTimeZoneLabelFromUTCOffset(item.StationInformation.UTCTimeOffset, isStandardTime);
                data.UTCOffset = item.StationInformation.UTCTimeOffset;
                data.DataRetrievedSuccessfully = true;
                if (peType.Equals("HG"))
                {
                    if (item.hasDischargeData.HasValue && item.hasDischargeData.Value && item.hasStageData.HasValue && item.hasStageData.Value)
                    {
                        // get discharge data
                        var dischargeData = _entities.DataItemObservations.Where(q => q.stationID == stationId && q.PEType == peType && q.isDischargeData == true && q.time.CompareTo(startDate) >= 0 && q.time.CompareTo(endDate) < 0).OrderBy(q => q.time);
                        data.ObservedUnit1Data = ConvertDataItemObservationsToDataPoints(dischargeData.ToList<DataItemObservation>());
                        data.YAxisLabelUnit1 = yAxisLabels["discharge"].ToString();
                        // get stage data
                        var stageData = _entities.DataItemObservations.Where(q => q.stationID == stationId && q.PEType == peType && q.isStageData == true && q.time.CompareTo(startDate) >= 0 && q.time.CompareTo(endDate) < 0).OrderBy(q => q.time);
                        data.ObservedUnit2Data = ConvertDataItemObservationsToDataPoints(stageData.ToList<DataItemObservation>());
                        data.YAxisLabelUnit2 = yAxisLabels["stage"].ToString();
                    }
                    else if (item.hasDischargeData.HasValue && item.hasDischargeData.Value == true)
                    {
                        // get discharge data
                        var dischargeData = _entities.DataItemObservations.Where(q => q.stationID == stationId && q.PEType == peType && q.isDischargeData == true && q.time.CompareTo(startDate) >= 0 && q.time.CompareTo(endDate) < 0).OrderBy(q => q.time);
                        data.ObservedUnit1Data = ConvertDataItemObservationsToDataPoints(dischargeData.ToList<DataItemObservation>());
                        data.YAxisLabelUnit1 = yAxisLabels["discharge"].ToString();
                    }
                    else if (item.hasStageData.HasValue && item.hasStageData.Value == true)
                    {
                        // get stage data
                        var stageData = _entities.DataItemObservations.Where(q => q.stationID == stationId && q.PEType == peType && q.isStageData == true && q.time.CompareTo(startDate) >= 0 && q.time.CompareTo(endDate) < 0).OrderBy(q => q.time);
                        data.ObservedUnit1Data = ConvertDataItemObservationsToDataPoints(stageData.ToList<DataItemObservation>());
                        data.YAxisLabelUnit1 = yAxisLabels["stage"].ToString();
                    }
                    else // error
                    {
                        log.Error(String.Format("GetChartDataFromObservedData:GetForDates: No specification of discharge or stage data for {0} {1}", stationId, peType));
                        data.DataRetrievedSuccessfully = false;
                    }
                }
                else
                {
                    var observedData = _entities.DataItemObservations.Where(q => q.stationID == stationId && q.PEType == peType && q.isDischargeData == true && q.time.CompareTo(startDate) >= 0 && q.time.CompareTo(endDate) < 0).OrderBy(q => q.time);
                    data.ObservedUnit1Data = ConvertDataItemObservationsToDataPoints(observedData.ToList<DataItemObservation>());
                    switch (peType)
                    {
                        case "QR":
                            data.YAxisLabelUnit1 = yAxisLabels["discharge"].ToString();
                            break;
                        case "HP":
                            data.YAxisLabelUnit1 = yAxisLabels["pool_elevation"].ToString();
                            break;
                        case "HF":
                            data.YAxisLabelUnit1 = yAxisLabels["forebay_elevation"].ToString();
                            break;
                        case "QI":
                            data.YAxisLabelUnit1 = yAxisLabels["inflow"].ToString();
                            break;
                        case "HM":
                            data.YAxisLabelUnit1 = yAxisLabels["tidal_stage"].ToString();
                            break;
                    }
                }
            }
            return data;
        }

        static List<DataPoint> ConvertDataItemObservationsToDataPoints(List<DataItemObservation> data)
        {
            List<DataPoint> dataPointList = new List<DataPoint>();
            foreach (DataItemObservation item in data)
            {
                DataPoint dp = new DataPoint(item.timeAsString, item.value);
                dataPointList.Add(dp);
            }
            return dataPointList;
        }
    }
}
