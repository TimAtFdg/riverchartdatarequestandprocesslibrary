﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiverChartDataRequestAndProcessLibrary
{
    public class UpdateCount
    {
        public void IncrementCountFor(string stationId, string peType)
        {
            using (var _entities = new RiverChartEntities())
            {
                DataItem dataItem = _entities.DataItems.Where(d => d.stationID.Equals(stationId) && d.PEType.Equals(peType)).FirstOrDefault();
                if (dataItem != null)
                {
                    if (dataItem.currentCount.HasValue)
                        dataItem.currentCount = dataItem.currentCount + 1;
                    else
                        dataItem.currentCount = 1;
                }

                _entities.SaveChanges();
            }
        }
    }
}
