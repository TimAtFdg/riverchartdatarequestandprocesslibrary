﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
//using System.Web;

namespace RiverChartDataRequestAndProcessLibrary
{
    [DataContract]
    public class DataPoint
    {
        [DataMember]
        public string XValue { get; set; }
        [DataMember]
        public double YValue { get; set; }

        public DataPoint(string x, double y)
        {
            XValue = x;
            YValue = y;
        }
    }
}