﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RiverChartDataRequestAndProcessLibrary
{
    public class SimpleDataItem
    {
        public string name { get; set; }
        public string PEType { get; set; }
        public string stationID { get; set; }
        public bool hasStageData { get; set; }
        public bool hasDischargeData { get; set; }

        public SimpleDataItem(vw_DataItemForSearch vwDI)
        {
            name = vwDI.name.Trim();
            PEType = vwDI.PEType;
            stationID = vwDI.stationID;
            if (vwDI.hasStageData.HasValue)
                hasStageData = vwDI.hasStageData == 1 ? true : false;
            else
                hasStageData = false;
            if (vwDI.hasDischargeData.HasValue)
                hasDischargeData = vwDI.hasDischargeData == 1 ? true : false;
            else
                hasDischargeData = false;
        }
    }
}