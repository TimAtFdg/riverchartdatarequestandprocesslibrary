﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;


// reference: http://msdn.microsoft.com/en-us/library/system.timezoneinfo.transitiontime.isfixeddaterule.aspx

namespace RiverChartDataRequestAndProcessLibrary
{
    static class DaylightSavingsTimeChecker
    {
        private static int utcOffset = -8;
        private static DateTime? _dstStart = null;
        private static DateTime? _dstEnd = null;
        public static DateTime? dstStart
        {
            get
            {
                if (_dstStart != null)
                    return _dstStart;
                else
                {
                    GetDSTStartAndEnd(utcOffset, DateTime.Now.Year, out _dstStart, out _dstEnd);
                    return _dstStart;
                }
            }
        }

        public static DateTime? dstEnd
        {
            get
            {
                if (_dstEnd != null)
                    return _dstEnd;
                else
                {
                    GetDSTStartAndEnd(utcOffset, DateTime.Now.Year, out _dstStart, out _dstEnd);
                    return _dstEnd;
                }
            }
        }

        public static bool IsStandardTime(DateTime time)
        {
            if (time.CompareTo(DaylightSavingsTimeChecker.dstStart) >= 0 && time.CompareTo(DaylightSavingsTimeChecker.dstEnd) < 0)
                return false;
            else
                return true;
        }

        public static DateTime ComputeAdjustedTime(DateTime inTime, int utcOffset, bool observesDaylightTime, out bool isStandardTime)
        {
            DateTime adjustedDateTime = inTime.AddHours((double)utcOffset);
            isStandardTime = true;
            if (observesDaylightTime == true)
            {
                //if (adjustedDateTime.CompareTo(DaylightSavingsTimeChecker.dstStart) < 0) // before dst
                //    return adjustedDateTime;
                //else if (adjustedDateTime.CompareTo(DaylightSavingsTimeChecker.dstEnd) >= 0) // after dst
                //    return adjustedDateTime;
                //else   //if (adjustedDateTime.CompareTo(DaylightSavingsTimeChecker.dstStart) >= 0)

                if (adjustedDateTime.CompareTo(DaylightSavingsTimeChecker.dstStart) >= 0 && adjustedDateTime.CompareTo(DaylightSavingsTimeChecker.dstEnd) < 0)
                {
                    adjustedDateTime = adjustedDateTime.AddHours(1.0);
                    isStandardTime = false;
                    if (adjustedDateTime.CompareTo(DaylightSavingsTimeChecker.dstEnd) >= 0)  // check for the last hour of dst which repeats as standard time
                    {
                        adjustedDateTime = adjustedDateTime.AddHours(-1.0);
                        isStandardTime = true;
                    }
                    //return adjustedDateTime;
                }
            }
            return adjustedDateTime;
        }

        private static void GetDSTStartAndEnd(int utcOffset, int year, out DateTime? dstStart, out DateTime? dstEnd)
        {
            DateTimeFormatInfo dateFormat = CultureInfo.CurrentCulture.DateTimeFormat;
            Calendar cal = CultureInfo.CurrentCulture.Calendar;
            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById(GetTimeZoneIDFromUTCOffset(utcOffset));
            TimeZoneInfo.AdjustmentRule[] adjustments = tzi.GetAdjustmentRules();
            int startYear = year;
            int endYear = startYear;
            if (adjustments.Length == 0)
            {
                dstStart = null;
                dstEnd = null;
                return;
            }
            else
            {
                TimeZoneInfo.AdjustmentRule adjustment = GetAdjustment(adjustments, year);
                if (adjustment == null)
                {
                    dstStart = null;
                    dstEnd = null;
                    return;
                }
                TimeZoneInfo.TransitionTime startTransition, endTransition;
                // Determine if starting transition is fixed 
                startTransition = adjustment.DaylightTransitionStart;
                // Determine if starting transition is fixed and display transition info for year
                if (startTransition.IsFixedDateRule)
                    dstStart = new DateTime(startYear, startTransition.Month, startTransition.Day, startTransition.TimeOfDay.Hour, startTransition.TimeOfDay.Minute, startTransition.TimeOfDay.Second);
                else
                    dstStart = GetDateTimeForYearMonthWeekDayOfWeek(startYear, startTransition.Month, startTransition.Week, startTransition.DayOfWeek, startTransition.TimeOfDay.Hour, startTransition.TimeOfDay.Minute, startTransition.TimeOfDay.Second);
                //DisplayTransitionInfo(startTransition, startYear, "Begins on");

                // Determine if ending transition is fixed and display transition info for year
                endTransition = adjustment.DaylightTransitionEnd;

                // Does the transition back occur in an earlier month (i.e., 
                // the following year) than the transition to DST? If so, make
                // sure we have the right adjustment rule.
                if (endTransition.Month < startTransition.Month)
                {
                    endTransition = GetAdjustment(adjustments, year + 1).DaylightTransitionEnd;
                    endYear++;
                }

                if (endTransition.IsFixedDateRule)
                    dstEnd = new DateTime(endYear, endTransition.Month, endTransition.Day, endTransition.TimeOfDay.Hour, endTransition.TimeOfDay.Minute, endTransition.TimeOfDay.Second);
                else
                    dstEnd = GetDateTimeForYearMonthWeekDayOfWeek(endYear, endTransition.Month, endTransition.Week, endTransition.DayOfWeek, endTransition.TimeOfDay.Hour, endTransition.TimeOfDay.Minute, endTransition.TimeOfDay.Second);
            }
        }

        private static TimeZoneInfo.AdjustmentRule GetAdjustment(TimeZoneInfo.AdjustmentRule[] adjustments, int year)
        {
            // Iterate adjustment rules for time zone
            foreach (TimeZoneInfo.AdjustmentRule adjustment in adjustments)
            {
                // Determine if this adjustment rule covers year desired
                if (adjustment.DateStart.Year <= year && adjustment.DateEnd.Year >= year)
                    return adjustment;
            }
            return null;
        }

        private static DateTime GetDateTimeForYearMonthWeekDayOfWeek(int year, int month, int week, DayOfWeek dayOfWeek, int hour, int minute, int second)
        {
            Calendar cal = CultureInfo.CurrentCulture.Calendar;
            DateTime dt = new DateTime(year, month, 1, hour, minute, second);
            int currentWeek = 0;
            if (cal.GetDayOfWeek(dt) == dayOfWeek)
                currentWeek++;
            while (currentWeek < week)
            {
                dt = dt.AddDays(1.0);
                if (cal.GetDayOfWeek(dt) == dayOfWeek)
                    currentWeek++;
            }
            return dt;
        }

        private static string GetTimeZoneIDFromUTCOffset(int utcOffset)
        {
            string timeZoneID = string.Empty;
            switch (utcOffset)
            {
                case -9:
                    timeZoneID = "Alaskan Standard Time";
                    break;
                case -8:
                    timeZoneID = "Pacific Standard Time";
                    break;
                case -7:
                    timeZoneID = "Mountain Standard Time";
                    break;
                case -6:
                    timeZoneID = "Central Standard Time";
                    break;
                case -5:
                    timeZoneID = "Eastern Standard Time";
                    break;
                default:
                    timeZoneID = "unknown";
                    break;
            }
            return timeZoneID;
        }

        public static string GetTimeZoneLabelFromUTCOffset(int utcOffset, bool isStandardTime)
        {
            string timeZoneID = string.Empty;
            switch (utcOffset)
            {
                case -9:
                    if (isStandardTime)
                        timeZoneID = "AKST"; //Alaskan Standard Time";
                    else
                        timeZoneID = "AKDT";
                    break;
                case -8:
                    if (isStandardTime)
                        timeZoneID = "PST";
                    else
                        timeZoneID = "PDT";
                    break;
                case -7:
                    if (isStandardTime)
                        timeZoneID = "MST";
                    else
                        timeZoneID = "MDT";
                    break;
                case -6:
                    if (isStandardTime)
                        timeZoneID = "CST";
                    else
                        timeZoneID = "CDT";
                    break;
                case -5:
                    if (isStandardTime)
                        timeZoneID = "EST";
                    else
                        timeZoneID = "EDT";
                    break;
                default:
                    timeZoneID = "";
                    break;
            }
            return timeZoneID;
        }
    }
}
