﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiverChartDataRequestAndProcessLibrary
{
    public static class StoreObservedData
    {
        public static void Store(string stationId, string peType, List<DataPoint>data, bool isDischargeData, bool isStageData)
        {
            if (data == null || data.Count==0)
                return;
            using (var _entities = new RiverChartEntities())
            {
                foreach (DataPoint dataPoint in data)
                {
                    DataItemObservation item = new DataItemObservation();
                    item.stationID = stationId;
                    item.PEType = peType;
                    item.isDischargeData = isDischargeData;
                    item.isStageData = isStageData;
                    item.value = dataPoint.YValue;
                    item.timeAsString = dataPoint.XValue;
                    item.time = DateTime.Parse(dataPoint.XValue);
                    _entities.DataItemObservations.Add(item);
                }
                _entities.SaveChanges();
            }

        }
    }
}
